package com.pran.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Search {
	
	static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	static Scanner sc=new Scanner(System.in);
	static Connection conn=MysqlConnection.getCon();
	
	/**
	 * The functionality to achieve from this method
	 * @throws Exception
	 */
	//choice which method is run
	public static void searchMain() throws Exception
	{
		Scanner sc=new Scanner(System.in);
    	System.out.println("          welcome to search Dashboard");
    	String ans;
    	do {
    		System.out.println("Enter your choice : \n\t1.GreaterAge and department. \n\t2.by Status is active. \n\t3.by Ctc is graterthan 2\n\t4.Exits ");
        	int ch=sc.nextInt();
        	switch(ch)
        	{
        	case 1:
        		searchByAgeAndDepartment();
        		break;
        	case 2:
        		searchByStatusAndDepartment();
        		break;
        	case 3:
        		searchByCtcAnddepartment();
        		break;
        	case 4:
        		exits();
        		break;
        	default :
        		System.out.println("Invalid input.");
        	}
        	System.out.println("Do you want to conti?(y/n)");
        	ans=sc.next();
    	}while(ans.equalsIgnoreCase("y"));
    	
	}
	
	//Given a department find all employees above a given age (Ex: above 40).
	public static void searchByAgeAndDepartment() throws IOException, Exception 
	{
		System.out.println("Enter the department :");
		String dname=br.readLine();
		System.out.println("enter the age: ");
		int age=sc.nextInt();
		
		Statement st=conn.createStatement();
		ResultSet rs=st.executeQuery("select * from employee where age >"+age+" and did =(select did from departmentdb where name='"+dname+"')");
		
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4)+" "+rs.getDouble(5)+""
					+rs.getDate(6)+" "+dname+" "+rs.getString(8));
		}
	
	}

	//Given a department find all active employees below a given age.
	public static void searchByStatusAndDepartment() throws Exception
	{
		System.out.println("Enter the department :");
		String dname=br.readLine();
		System.out.println("enter the age: ");
		int age=sc.nextInt();
		
		Statement st=conn.createStatement();
		ResultSet rs=st.executeQuery("select * from employee where age <"+age+" and status='active' and did =(select did from departmentdb where name='"+dname+"')");
		
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4)+" "+rs.getDouble(5)+""
					+rs.getDate(6)+" "+dname+" "+rs.getString(8));
		}
	}

	//Given a department find all active employees below a given age having CTC greater than 2 lakh
	public static void searchByCtcAnddepartment() throws Exception
	{
		System.out.println("Enter the department :");
		String dname=br.readLine();
		System.out.println("enter the age: ");
		int age=sc.nextInt();
		
		Statement st=conn.createStatement();
		ResultSet rs=st.executeQuery("select * from employee where age <"+age+" and ctc >2 and status='active' and did =(select did from departmentdb where name='"+dname+"')");
		
		while(rs.next())
		{
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getInt(4)+" "+rs.getDouble(5)+""
					+rs.getDate(6)+" "+dname+" "+rs.getString(8));
		}
	}

	static void exits()
	{
		System.out.println("Thankyou ...have a nice day");
		System.exit(0);
	}
}
