package com.pran.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * EmployeeDao class contain 3 method
 * @author Shubham.Mhadgut
 *
 */
public class EmployeeDao {
	
	static Connection conn=MysqlConnection.getCon();
	static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	static Scanner sc=new Scanner(System.in);
	
	//choice which method is run
	/**
	 * Main method of employee where we can choose the operation
	 * @throws ParseException
	 * @throws IOException
	 */
	public static void empMain() throws ParseException, IOException
	{
		Scanner sc=new Scanner(System.in);
    	System.out.println("          welcome to employee Dashboard");
    	String ans;
    	do {
    		System.out.println("Enter your choice : \n\t1.Add employee. \n\t2.show all employee. \n\t3.Exits ");
        	int ch=sc.nextInt();
        	switch(ch)
        	{
        	case 1:
        		addEmp();
        		break;
        	case 2:
        		showEmp();
        		break;
        	case 3:
        		exits();
        		break;
        	default :
        		System.out.println("Invalid input.");
        	}
        	System.out.println("Do you want to conti?(y/n)");
        	ans=sc.next();
    	}while(ans.equalsIgnoreCase("y"));
    	
		
	}
	
	//add employee details
	/**
	 * add employee details
	 * @throws ParseException
	 * @throws IOException
	 */
	public static void addEmp() throws ParseException, IOException
	{
		System.out.println("   Enter the employee details:   ");
		System.out.print("Enter the employee name: ");
		String name=br.readLine();
		System.out.print("Enter the password for login:");
		String pass=br.readLine();
		System.out.print("Enter the age :");
		int age=sc.nextInt();
		System.out.print("Enter the annual ctc:");
		double ctc=sc.nextDouble();
		System.out.print("Enter the joining date (yyyy-mm-dd)");
		String date=sc.next();
		//convert date into sql date format
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // your template here
		java.util.Date dateStr = formatter.parse(date);
		java.sql.Date dateDB = new java.sql.Date(dateStr.getTime());
		
		System.out.print("Enter the department name:");
		String dname=br.readLine();
		int did=0;
		try {
			Statement st=conn.createStatement();
			ResultSet rs=st.executeQuery( "select did from departmentdb where name='"+dname+"'");
			while(rs.next())
			{
				did=rs.getInt(1);
			}
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		System.out.print("Enter the status of employee: ");
		String status=sc.next();
		
		System.out.println("  Enter the address of employee:");
		System.out.print("Enter the city:");
		String city=sc.next();
		int pincode=0;
		String num=null;
		do {
			System.out.print("Enter the pincode:");
			pincode=sc.nextInt();
			num=Integer.toString(pincode);	
		}while(num.length()>6);
		
		System.out.print("Enter the state:");
		String state=sc.next();
		
		System.out.print("Enter the country:");
		String country=sc.next();
		
		try {
			Statement st1=conn.createStatement();
			int result=st1.executeUpdate("insert into employee(uname,pass,age,ctc,jdate,did,status) values ('"+name+"','"+pass+"',"+age+","+ctc+""
					+ ",'"+dateDB+"',"+did+",'"+status+"')");
			
			if(result>0) {
				System.out.println("Details add successfully..........");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			int lastid=0;
			Statement st2=conn.createStatement();
			//extract last id of employee table
			ResultSet rs2=st2.executeQuery("select eid from employee ORDER BY eid DESC LIMIT 1" );
			while(rs2.next()) {
				lastid = rs2.getInt(1);
			}
			
			int result2=st2.executeUpdate("insert into eaddress(city,pincode,state,country,eid) values ('"+city+"',"
			+pincode+",'"+state+"','"+country+"',"+lastid+")"); 
			
			if(result2>0)
			{
				System.out.println("address is add successfully....");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	//show employee data
	/**
	 * show employee details
	 */
	public static void showEmp()

	{
		Statement st;
		try {
			st = conn.createStatement();
			ResultSet rs=st.executeQuery("select * from employee");
			while(rs.next())
			{
				
				System.out.println("Id: "+rs.getInt(1)+" Name: "+rs.getString(2)+" Password: "+rs.getString(3)+"\n Age: "+rs.getInt(4)+""
						+ " Annual CTC: "+rs.getDouble(5)+" Date of joining: "+rs.getDate(6)+"\n Department ID: "+rs.getInt(7)+" Status: "+rs.getString(8));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	static void exits()
	{
		System.out.println("Thankyou ...have a nice day");
		System.exit(0);
	}
}


