package com.pran.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;
/**
 * departmentdao contain 3 methods
 * @author Shubham.Mhadgut
 *
 */
public class DepartmentDao {
	
	
	static Connection conn=MysqlConnection.getCon();
	static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	/**
	 *  we can choose the operation from these method
	 * @throws IOException
	 */
	//choice which method is run
	public static void depMain() throws IOException
	{
		Scanner sc=new Scanner(System.in);
    	System.out.println("          welcome to department Dashboard");
    	String ans;
    	do {
    		System.out.println("Enter your choice : \n\t1.Add department. \n\t2.show all department. \n\t3.Exits ");
        	int ch=sc.nextInt();
        	switch(ch)
        	{
        	case 1:
        		addData();
        		break;
        	case 2:
        		showData();
        		break;
        	case 3:
        		exits();
        		break;
        	default :
        		System.out.println("Invalid input.");
        	}
        	System.out.println("Do you want to conti?(y/n)");
        	ans=sc.next();
    	}while(ans.equalsIgnoreCase("y"));
    	
		
	}
	
	/**
	 * Add department details
	 * @throws IOException
	 */
	//insert data into department table
	public static void addData() throws IOException
	{
		System.out.print("Enter the department name:");
		String Name=br.readLine();
		System.out.println("Enter the region:");
		String Region=br.readLine();
		
		try {
			Statement st=conn.createStatement();
			int result=st.executeUpdate("insert into departmentdb(name,region) values ('"+Name+"','"+Region+"')");
			if(result>0)
			{
				System.out.println("Data insert successfully");
			}
			else
			{
				System.out.println("Data is not insert");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Show all department
	 */
	//show data of department table
	public static void showData()
	{
		try {
			Statement st=conn.createStatement();
			ResultSet rs=st.executeQuery("select * from departmentdb");
			System.out.println("Department details: ");
			System.out.println("Id       Name            Region");
			while(rs.next())
			{
				System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getString(3));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	static void exits()
	{
		System.out.println("Thankyou ...have a nice day");
		System.exit(0);
	}
	
}
