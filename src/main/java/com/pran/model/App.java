package com.pran.model;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

/**
 * This main method 
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception 
    {
    	Scanner sc=new Scanner(System.in);
    	System.out.println("          welcome to company Dashboard");
    	String ans;
    	do {
    		System.out.println("Enter your choice : \n\t1.Add employee details. \n\t2.Add department details. \n\t3.Search \n\t4.Exits ");
        	int ch=sc.nextInt();
        	switch(ch)
        	{
        	case 1:
        		EmployeeDao.empMain();
        		break;
        	case 2:
        		DepartmentDao.depMain();
        		break;
        	case 3:
        		Search.searchMain();
        		break;
        	case 4:
        		exits();
        		break;
        	default :
        		System.out.println("Invalid input.");
        	}
        	System.out.println("Do you want to conti?(y/n)");
        	ans=sc.next();
    	}while(ans.equalsIgnoreCase("y"));
    
    }
    
    static void exits()
	{
		System.out.println("Thankyou ...have a nice day");
		System.exit(0);
	}
    
}
