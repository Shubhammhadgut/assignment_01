//
username: root
password: smarte

//Create database in mysql
create database CompanyDetails;

//select database
use companydetails;

//create table for department
create table departmentdb(
did int auto_increment PRIMARY KEY,
name varchar(45) not null,
region varchar(45)
);


//create table for employee
create table employee(
eid int auto_increment primary key,
uname varchar(50),
pass varchar(50),
age int,
ctc double,
jdate date,
did int,
status varchar(20),
FOREIGN KEY (did) REFERENCES departmentdb(did)
);

//create table for address
create table eaddress(
aid int auto_increment primary key,
city varchar(50),
pincode int,
state varchar(100),
country varchar(100),
eid int,
foreign key(eid) references employee(eid)
);

//department details
Department details: 
Id       Name                Region
1 	Accounts and Finance     Mumbai
2 	HR                       Mumbai
3	Research and development Mumbai
4 	Learning and development Mumbai











