-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: companydetails
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee` (
  `eid` int NOT NULL AUTO_INCREMENT,
  `uname` varchar(50) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `ctc` double DEFAULT NULL,
  `jdate` date DEFAULT NULL,
  `did` int DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`eid`),
  KEY `did` (`did`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`did`) REFERENCES `departmentdb` (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Shubham Mhadgut','shubham',21,2.32,'2020-02-27',3,'active'),(2,'Omkar Mhadgut','omkar',34,3.45,'2020-01-02',3,'active'),(3,'Vinita More','vinita',45,4.21,'2005-03-01',2,'active'),(4,'Vaibhav Mane','vaibhav',45,4.54,'2001-03-21',2,'Active'),(5,'Vaishali Tambe','vaishali',50,7.74,'2000-06-21',2,'Active'),(6,'Vijay Tambe','vijay',55,10.74,'2000-03-21',2,'Active'),(7,'Vijay kamble','vijayk',60,10,'2000-03-21',2,'Active'),(8,'Mayur Mahadik','mayur',52,8.67,'2002-04-06',2,'Active'),(9,'Sai Mahadik','sai',32,5.32,'2004-03-30',2,'Active'),(10,'Rohan Mahamunkar','rohan',23,3.43,'2005-05-30',2,'Inactive'),(11,'Amit Mahadik','amit',26,4.43,'2006-03-30',2,'Inactive'),(12,'Amit surve','amits',22,3.86,'2007-01-30',2,'Inactive'),(13,'Shravni surve','shravni',22,3.86,'2020-05-30',3,'Inactive'),(14,'Akansha salve','akansha',25,3.21,'2020-02-28',3,'Inactive'),(15,'Pruthavi walse','pruthavi',25,4.21,'2020-01-28',3,'Inactive'),(16,'Sai mahamunkar','saim',23,2.21,'2020-01-28',3,'Inactive'),(17,'Amol Mane','amol',32,5.21,'2018-01-28',3,'active'),(18,'Shubham Mhadgut','shubhamm',30,6.21,'2019-05-28',3,'active'),(19,'Ankita Mahale','ankita',31,7.21,'2017-05-25',3,'active'),(20,'Pranjal More','pranjal',31,8.21,'2017-05-25',3,'active');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 22:01:45
