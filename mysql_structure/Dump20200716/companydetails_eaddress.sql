-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: companydetails
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eaddress`
--

DROP TABLE IF EXISTS `eaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eaddress` (
  `aid` int NOT NULL AUTO_INCREMENT,
  `city` varchar(50) DEFAULT NULL,
  `pincode` int DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `eid` int DEFAULT NULL,
  PRIMARY KEY (`aid`),
  KEY `eid` (`eid`),
  CONSTRAINT `eaddress_ibfk_1` FOREIGN KEY (`eid`) REFERENCES `employee` (`eid`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eaddress`
--

LOCK TABLES `eaddress` WRITE;
/*!40000 ALTER TABLE `eaddress` DISABLE KEYS */;
INSERT INTO `eaddress` VALUES (1,'Mumbai',400043,'maharashtra','india',1),(2,'mumbai',4021020,'active','indian',2),(3,'Pune',230092,'maharashtra','india',3),(4,'Mumbai',400202,'maharashtra','india',4),(5,'Mumbai',400200,'maharashtra','india',5),(6,'Mumbai',400205,'maharashtra','india',6),(7,'Mumbai',401205,'maharashtra','india',7),(8,'Mumbai',401805,'maharashtra','india',8),(9,'Ratnagiri',401345,'maharashtra','india',9),(10,'Malad',401305,'maharashtra','india',10),(11,'kalyan',402305,'maharashtra','india',11),(12,'chipalun',405308,'maharashtra','india',12),(13,'kankavli',405212,'maharashtra','india',13),(14,'karjat',403412,'maharashtra','india',14),(15,'nagpur',403892,'maharashtra','india',15),(16,'nashik',409892,'maharashtra','india',16),(17,'sagali',408492,'maharashtra','india',17),(18,'mahad',402301,'maharashtra','india',19),(19,'kolad',402307,'maharashtra','india',19),(20,'devrukh',402387,'maharashtra','india',20);
/*!40000 ALTER TABLE `eaddress` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 22:01:46
